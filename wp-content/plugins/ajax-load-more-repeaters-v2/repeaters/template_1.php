<div class="post-container <?php if (!has_post_thumbnail()) { ?> no-img<?php } ?>">    
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
    <div class="post-thumb"><?php if ( has_post_thumbnail() ) { the_post_thumbnail('post-thumbnail'); }?></div>
    <div class="post-content">
        <h3 class="post-title"><?php the_title(); ?></h3>
    	<p class="publish-date"><?php the_time('F d, Y'); ?></p>
        <div class="post-desc">
    		<p><?php echo excerpt(15); ?></p>
    	</div>
    	<div class="border"></div>
   </div>
   </a>
</div>