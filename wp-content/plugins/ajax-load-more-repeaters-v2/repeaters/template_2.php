<div class="video-item">
   <?php $youtube = get_field('youtube_id'); ?>
   <?php $tags = get_the_tags();
		$tag_template = '';
		foreach($tags as $tag) {
    		$tag_template .= "<span>#$tag->name</span>";
		}
	?>

   <a href="https://www.youtube.com/watch?v=<?php echo $youtube; ?>" class="mfp-iframe lightbox" rel="lightbox" title="<?php the_title(); ?>">
      <div class="image-wrapper">
      	<img src="http://img.youtube.com/vi/<?php echo $youtube; ?>/mqdefault.jpg" style="width: 100%;">
   	  </div>
      <div class="post-info">	
         <h3><?php the_title(); ?></h3>
      	 <p class="publish-date"><?php the_time('F d, Y'); ?></p>
         <div class="tags"><?php echo $tag_template ?></div>
         <div class="border-bottom"></div>
      </div>
   </a>
</div>