<?php

require_once 'theme-settings.php';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Add style from parent theme
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}
 
// Add "custom CSS" field for Enfold builder elements
add_theme_support('avia_template_builder_custom_css');

//Disable themes, plugins and WP core update notifications
// function remove_core_updates(){
// 	global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
// }
// add_filter('pre_site_transient_update_core','remove_core_updates');
// add_filter('pre_site_transient_update_plugins','remove_core_updates');
// add_filter('pre_site_transient_update_themes','remove_core_updates');

// Add language file for Enfold theme
function avia_lang_setup()
{
    $lang = get_stylesheet_directory()  . '/languages';
    load_child_theme_textdomain('avia_framework', $lang);
}
add_action('after_setup_theme', 'avia_lang_setup');

add_filter('avf_builder_boxes','custom_post_types_options');

function custom_post_types_options($boxes)
{

	$boxes = array(
		array( 'title' =>__('Avia Layout Builder','avia_framework' ), 'id'=>'avia_builder', 'page'=>array('portfolio','page','post', 'qa'), 'context'=>'normal', 'priority'=>'high', 'expandable'=>true ),
		array( 'title' =>__('Layout','avia_framework' ), 'id'=>'layout', 'page'=>array('portfolio', 'page' , 'post', 'qa'), 'context'=>'side', 'priority'=>'low'),
		array( 'title' =>__('Additional Portfolio Settings','avia_framework' ), 'id'=>'preview', 'page'=>array('portfolio'), 'context'=>'normal', 'priority'=>'high' ),
		array( 'title' =>__('Breadcrumb Hierarchy','avia_framework' ), 'id'=>'hierarchy', 'page'=>array('portfolio'), 'context'=>'side', 'priority'=>'low'),
	);

	return $boxes;
}

function excerpt( $limit ) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/[.+]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function wpb_add_google_fonts() {
    wp_enqueue_style( 'wpb-google-fonts-roboto', '//fonts.googleapis.com/css?family=Roboto:400,700&amp;subset=vietnamese', false ); 
    wp_enqueue_style( 'wpb-google-fonts-condensed', '//fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=vietnamese', false ); 
    
    wp_enqueue_script('jquery-scrollbar-js', get_stylesheet_directory_uri() . '/js/jquery-scrollbar.js' );
    wp_enqueue_script('site-js', get_stylesheet_directory_uri() . '/js/site.js' );
}

add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );

add_action('ava_before_footer','avia_above_footer');
function avia_above_footer(){
    dynamic_sidebar('mobile_newsletter');
    dynamic_sidebar('newsletter_widget');
}

// add_filter( 'wp_nav_menu_items', 'email_menu_item', 10, 2 );
// function email_menu_item ( $items, $args ) {
//     if ($args->menu->slug == 'main-menu') {
//         $items .= '<li id="menu-item-email" class="noMobile menu-item menu-item-avia-special"><a href="mailto:tien@zodinet.com"></a></li>';
//     }
//     return $items;
// }

function show_latest_video( $atts ) {
	$atts = shortcode_atts( array(
		'foo' => 'no foo',
		'baz' => 'default baz'
    ), $atts, 'bartag' );
    
    $recent_posts = wp_get_recent_posts(array(
        'post_type'=>'video',
        'numberposts' => 1,
        'post_status' => 'publish'
    ));

    if(count($recent_posts) == 1) {
        $latest_video = $recent_posts[0];
        $youtube = get_field('youtube_id', $latest_video['ID']);
        $title = $latest_video['post_title'];
        $tags = get_the_tags($latest_video['ID']);
        $tag_template = "";
        foreach($tags as $tag) {
            $tag_template .= "<span>#$tag->name</span>";
        }
        $template = "
        <div class='latest-video'>
            <a href='https://www.youtube.com/watch?v=$youtube' class='mfp-iframe lightbox' rel='lightbox' title='$title'>
                <div class='image-wrapper'>
                    <div class='stretchy-wrapper'>
                        <div>
                            <img src='http://img.youtube.com/vi/$youtube/hqdefault.jpg' style='overflow: hidden; width:100%;'/>
                        </div>
                    </div>
                    <div class='post-content-data'>	
                        <h3>$title</h3>
                        <div class='tag-template'>
                            $tag_template
                        </div>
                    </div>
                </div>
            </a>
        </div>";
        return $template;
    }
	return "";
}
add_shortcode( 'latest_video', 'show_latest_video' );

add_filter('avf_markup_helper_attributes', 'avf_markup_helper_attributes_modified', 10, 2);
function avf_markup_helper_attributes_modified($attributes, $args) {
    if($args['context'] == 'image') {
        $attributes['itemprop'] = 'image';
    }
    return $attributes;
}

function print_menu_shortcode($atts, $content = null) {
    extract(shortcode_atts(array( 'name' => null, ), $atts));
    return wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
}
add_shortcode('menu', 'print_menu_shortcode');

function remove_author_publish_posts(){

    // $wp_roles is an instance of WP_Roles.
    global $wp_roles;
    $wp_roles->remove_cap( 'editor', 'publish_posts' );
}

add_action( 'init', 'remove_author_publish_posts' );

?>
