<?php
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config, $more;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 get_header();
	
		
		$showheader = true;
		if(avia_get_option('frontpage') && $blogpage_id = avia_get_option('blogpage'))
		{
			if(get_post_meta($blogpage_id, 'header', true) == 'no') $showheader = false;
		}
		
	 	if($showheader)
	 	{
			echo avia_title(array('title' => avia_which_archive()));
		}
		
		do_action( 'ava_after_main_title' );
	?>

		<div class='container_wrap container_wrap_first main_color <?php avia_layout_class( 'main' ); ?>'>

			<div class='container template-blog '>

				<main class='content <?php avia_layout_class( 'content' ); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'post'));?>>
                    <?php
                        $queried_object = get_queried_object();

                        $taxonomy = $queried_object->taxonomy;
                        $term_id = $queried_object->term_id;

                        // Get field value
                        $is_qa = get_field('is_qa_category', $taxonomy.'_'.$term_id);

                        $category = get_category( get_query_var( 'cat' ) );
                        
                    ?>

                    <?php 
                        if ($is_qa) {
                            ?>
                                <div class="avia_textblock common-title " itemprop="text">
                                    <p><?php echo $category->cat_name ?></p>
                                </div>
                                <div class="avia_textblock common-sub-title " itemprop="text">
                                    <p>Câu hỏi thường gặp</p>
                                </div>
                                <div class="home-latest-news">
                                <?php echo do_shortcode('[ajax_load_more id="6057501332" container_type="div" css_classes="qa-posts" repeater="template_3" post_type="qa" category="'.$category->slug.'" button_label="XEM THÊM" posts_per_page="4" scroll="false"]') ?>
                                </div>

                                <div class="avia_textblock common-sub-title " itemprop="text">
                                    <p>Bạn có thắc mắc về tài chính? Hãy liên hệ chúng tôi!</p>
                                </div>
                                <div class="qa-form">
                                    <?php echo do_shortcode('[contact-form-7 id="1203" title=""]'); ?>
                                </div>
                            <?php
                        } else {

                            $post_category = null;

                            if ($category->parent) {
                                $top_cat = get_category($category->parent);
                                $post_category = $category;
                            } else {
                                $top_cat = $category;
                            }
                    ?>

                            <div class="category-name">
                                <?php echo $top_cat->cat_name ?>
                            </div>

                            <?php
                            $template = "";

                            $args = array('parent' => $top_cat->cat_ID, 'hide_empty' => 0);
                            $categories = get_categories( $args );

                            // This is posts of parent category
                            $posts = get_posts('numberposts=5&category__in='.$top_cat->cat_ID);
                            

                            if (count($posts) > 0) {
                                foreach ($posts as $post) {
                                    $thumbnail_id = get_post_thumbnail_id($post->ID);
                                    $template .= "[av_slide slide_type='image' id='".$thumbnail_id."' attachment=',' video='' mobile_image='' video_ratio='' 
                                    title='".$post->post_title."' link_apply='image' link='post,".$post->ID."' link_target='' button_label='Xem thêm'][/av_slide]";
                                }
                                
                                $template = "[av_slideshow size='magazine' animation='slide' autoplay='true' interval='4']" . $template . "[/av_slideshow]";
                                
                                echo do_shortcode($template);
                            }
                            

                            if (count($categories) > 0) {
                                $load_more_id = rand(0, 1000000);

                                if($post_category == null) {
                                    $post_category = $categories[0];
                                }

                                $tab_template = '<div class="tabcontainer"><ul class="tab_titles">';
                                foreach ($categories as $sub) {
                                    $class = '';
                                    if($sub->cat_ID == $post_category->cat_ID) {
                                        $class = 'active_tab';
                                    }
                                    $tab_template .= '<li class="tab ' . $class . '"><a href="' . get_category_link( $sub->cat_ID ) . '" >' . $sub->cat_name . '</li>';
                                }
                                $tab_template .= '</ul></div>';

                                $post_template = '[ajax_load_more id="'.$load_more_id.'" container_type="div" posts_per_page="6" css_classes="category-posts" post_type="post" button_label="Xem thêm" category="'.$post_category->slug.'" cache="false" cache_id="cache-'.$post_category->slug.'"]';
            
                                $tab_template .= $post_template;
            
                                echo do_shortcode($tab_template);
                            }

                            ?>

                    <?php } ?>
                    

				<!--end content-->
				</main>

				<?php

				//get the sidebar
				// $avia_config['currently_viewing'] = 'blog';
				get_sidebar();

				?>

			</div><!--end container-->

		</div><!-- close default .container_wrap element -->




<?php get_footer(); ?>
