jQuery(function($){
    var promotionBanner = $('.promotion-banner-container');
    var newsletterBanner = $('.sidebar-newsletter-container');
    

    if(promotionBanner.length != 0) {
        var offset = promotionBanner.offset();
        var stop = (offset.top - 40);

        var stickyTop = stop;

        window.addEventListener('resize', function () {
            offset = promotionBanner.offset();
            stop = (offset.top - 40);
        })

        window.onscroll = function (e) {
            var windowTop = $(window).scrollTop();

            //show-hide-sidebar-newsletter
            var b = document.querySelector('.newsletter-footer-widget').getBoundingClientRect(),
                a = document.querySelector('.sidebar-newsletter-container').getBoundingClientRect();

            if (b.top <= a.top + a.height && b.top + b.height > a.top) {
                $('.sidebar-newsletter-container').addClass('overlapse-hide');
            } else {
                $('.sidebar-newsletter-container').removeClass('overlapse-hide');
            }

            //show-hide-promotion-banner
            var c = document.querySelector('.promotion-banner-container').getBoundingClientRect();
            if (b.top <= c.top + c.height && b.top + b.height > c.top) {
                $('.promotion-banner-container').addClass('overlapse-hide');
            } else {
                $('.promotion-banner-container').removeClass('overlapse-hide');
            }

            if (stickyTop < windowTop && $(".main_color.container_wrap_first ").height() + $(".main_color.container_wrap_first").offset().top - promotionBanner.height() > windowTop) {
                var sidebarWidth = $('.sidebar.sidebar_right .image').width();
                
                promotionBanner.addClass('stick');
                promotionBanner.css('left', offset.left);
                promotionBanner.width(sidebarWidth);

                newsletterBanner.addClass('stick');
                newsletterBanner.css('left', offset.left);
                newsletterBanner.css('top', 90 +  promotionBanner.height());
                newsletterBanner.width(sidebarWidth);
                
            } else {
                promotionBanner.removeClass('stick');
                promotionBanner.css('left', 0);
                promotionBanner.width('100%');

                newsletterBanner.removeClass('stick');
                newsletterBanner.css('left', 0);

                newsletterBanner.css('left', 0);
                newsletterBanner.css('top', 0);
                newsletterBanner.width('100%');
            }

        }
    }

    function setCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    var singleBlog = $('.template-single-blog'); 
    if(singleBlog.length != 0) {
        $("#progress-bar").onscroll({backgroundColor: '#00a8ff', height: '8px', position: 'fixed', top: 0, 'z-index': 12 });
    }

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            $("body").addClass("stick-header");
        } else {
            $("body").removeClass("stick-header");
        }
    });

    var hideNewsletter = getCookie('hide_newsletter') == 'true';
    if(hideNewsletter) {
        $('.mobile-newsletter-container').hide();
    }

    $('.mobile-newsletter-container .btn-register').click(function(){
        $(this).hide();
        $('.mobile-newsletter-container .yikes-mailchimp-container').show();
    })

    $('.mobile-newsletter-container .close-button').click(function(){
        $('.mobile-newsletter-container').hide();
        setCookie('hide_newsletter', 'true');
    });

    $('.main_menu').on('click', '#av-burger-menu-ul li', function(){
        var item = $(this);
        var friends = item.siblings('.av-show-submenu');
        friends.removeClass('av-show-submenu');
        
        $('.sub-menu', friends).hide();
        
    });
});