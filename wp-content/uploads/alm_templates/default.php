<div <?php if (!has_post_thumbnail()) { ?> class="no-img"<?php } ?>>
   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
      <div class="image-wrapper">
   	  <?php if ( has_post_thumbnail() ) { the_post_thumbnail('medium'); }?>
   	  </div>
      <div class="post-info">	
         <h3><?php the_title(); ?></h3>
         <p style="margin-bottom: 5px;" class="publish-date"><?php the_time('F d, Y'); ?></p>
 
	<?php echo excerpt(20); ?>
      </div>
   </a>
</div>
